# atomic_broadcast

Нужно реализовать доставку сообщений всем участникам группы (broadcast) в едином глобальном порядке. 

## Модель

Процессы соответствуют модели fail-recovery -- процессы имеют доступ к persistent storage, процессы могут падать и восстанавливаться после падерний с сохранением persistent storage.

Линки между процессами - PerfectLink. Сообщения могут задерживаться на произвольный период времени.

Формат сообщения передаваемого через линки - произвольный.

Сообщения передаваемые через broadcast описываются своими идентификаторами типа int. Все идентификаторы в рамках теста уникальны.

## Задание

Задача состоит из двух частей:

1. Придумайте тесты, ломающие неправильную реализацию
  * Тесты нужно вписать в файл `tests/extra.go`.
2. Внесите правки в неправильную реализацию, чтобы получилась правильная. Ваша реализация должна находиться в файле `algo/solution.go`
  * Реализация должна проходить ваши тесты.
  * Реализация должна проходить закрытые тесты на сервере.

## Реализация групповой доставки

Участники группы реализуют интерфейс `AtomicBroadcaster`.
 - `Broadcast(messageId int)` вызывается, когда участник решил послать сообщение с идентификатором `messageId` всей группе.
 - `OnMessage(message interface{}, source int)` вызывается, когда нижележащий транспорт
  доставил сообщение `message` от участника `source`
 
Функция создающая участника группы имеет сигнатуру `
type AtomicBroadcasterCtor func(nodeId, nodeCount int, deliver Deliver, context Context) AtomicBroadcaster`. Здесь:
 - `nodeId` - номер участника;
 - `nodeCount` - число участников;
 - `deliver` - коллбек, через который участник доставляет сообщения, для которых надежно определен порядок;
 - `context` - интерфейс реализующий транспорт и персистентное хранилище.

В файле `algo/example.go` находится пример неправильной реализации. Изучите его.
Ваш код должен быть в файле `algo/solution.go`. Правильная реализация потенциально требует добавления всего 20-30 строк кода. Вы можете максимально переиспользовать код существующей реализации. Если вы отправляете реализацию, поставьте переменную `HasSolution` в `true`. 

## Описание теста

Описание теста состоит из сегментов.
В начала сегмента идет этап warm up, на котором не происходит вызовов `Broadcast`, и для определения задержки доставки сообщений используется `StableDelayFunc`. Этот период нужен чтобы "стабилизировать" состояние системы после предыдущего сегмента.

Затем идет этап stable, в течение которого соствав живых и мертвых нод не меняется, для определения задержки доставки сообщений используется `StableDelayFunc`. В течение этого периода происходят вызовы `Broadcast`, описанные в `StableBroadcasts`. Таймстемпы времени вызовов задаются отностельно момента начала этапа. Этап завершается, когда для каждого сообщения отправленного в течение этапа через broadcast, был совершен соответствующий вызов Deliver на каждой из живых нод.

Третий этап (unstable) начинается сразу по окончании второго. На третьем этапе ноды могут умирать и возвращаться (описывается с помощью `DeathEvents` и `ReviveEvents`), для определения задержки доставки сообщений используется `UnstableDelayFunc`. Сегмент завершается когда были исполнены все `DeathEvents` и `ReviveEvents` и отправлены все сообщения из `UnstableBroadcasts`.

На момент окончания сегмента в живых должно оставаться больше половины нод (чтобы был живой кворум).

По окончании последнего сегмента поднимаются все ноды и просходит этап teardown в течение `TeardownRounds * AvgDeliveryTime`, после которого валидируются полученные результаты.

Корректный тест должен удовлетворять всем ограничениям (`MaxBroadcastsPerTest`, `MaxEventsPerTest`, `MaxSegmentsPerTest`, `MaxNodeCount`), содержать корректные `DelayFunc` и не вызывать `Broadcast` на мертвой ноде.

Тест описывается структурой `TestDescriptor`.
 - Поле `Author` содержит email.
 - Поле `TestName` содержит человекочитаемое описание теста.
 - Поле `NodeCount` задаёт количество участников группы.
 - Поле `Segments` содержит описание сегментов теста.

Описание сегментов
 - Поле `StableBroadcasts` - список сообщений отправляемых в стабильном этапе данного сегмента; таймстемпы моментов отправки задаются относительно начала стабильного этапа.
 - Поле `StableDelayFunc` - задаёт функцию, определяющую время достави сообщения в стабильном этапе
 - Поле `UnstableBroadcasts` - список сообщений отправляемых в нестабильном этапе данного сегмента; таймстемпы моментов отправки задаются относительно начала нестабильного этапа..
 - Поле `UnstableDelayFunc` - задаёт функцию, определяющую время достави сообщения в unstable этапе.
 - `DeathEvents`, `RevivalEvents` - моменты когда ноды умирают и возвращаются; таймстемпы моментов отправки задаются относительно начала нестабильного этапа.

 `DelayFunc` может  посмотреть на `SendTimestamp` и предлагаемый `DeliveryTimestamp` сообщения и вернуть `SendTimestamp + N`, где `N` задержка доставки. Никакой `DelayFunc` не имеет права отправлять сообщения "в прошлое". Для стабильного этапа, задержка полученная через `DelayFunc` не должна отличаться от предлагаемой более чем на 10%.

 
В файле `tests/example.go` находятся примеры тестов. Изучите их.
Ваши тесты должны быть в файле `tests/extra.go`.

## Тестирование решения

Чтобы запустить публичные тесты (`public_test.go`), выполните следующую команду
находясь в директории с задачей.

```
$ go test -v gitlab.com/slon/shad-ds/causal_broadcast
```

## Сдача решения

Выполните следующую команду, находясь в директории с задачей.

```
$ python3 ../submit.py
```
