package causal_broadcast

import (
	"gitlab.com/slon/shad-ds/causal_broadcast/algo"
	"gitlab.com/slon/shad-ds/causal_broadcast/lib"
	"gitlab.com/slon/shad-ds/causal_broadcast/tests"
	"testing"
)

func TestExampleIsPassingExampleTests(t *testing.T) {
	lib.RunTests(t, algo.NewExample, tests.ExampleTests, false, true)
}

func TestExampleIsNotPassingExtraTests(t *testing.T) {
	lib.RunTests(t, algo.NewExample, tests.ExtraTests, true, true)
}

func TestSolutionIsPassingExampleTests(t *testing.T) {
	if algo.HasSolution {
		lib.RunTests(t, algo.NewSolution, tests.ExampleTests, false, true)
	}
}

func TestSolutionIsPassingExtraTests(t *testing.T) {
	if algo.HasSolution {
		lib.RunTests(t, algo.NewSolution, tests.ExtraTests, false, false)
	}
}
