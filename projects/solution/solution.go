package solution

import (
	"reflect"

	"gitlab.com/slon/shad-ds/projects/interfaces"
)

/////////////////////////////////////////////////////////////////////////////////////////////

type ProjectsService struct {
	context interfaces.ActorContext
}

func (s *ProjectsService) Receive(message interfaces.Message) {
	switch m := message.Payload.(type) {
	case *interfaces.CreateProjectReq:
		s.context.Log().Debugw(
			"Received create project request",
			"from", message.Sender,
			"to", interfaces.ProjectsActorId,
			"request_id", m.RequestId,
			"project", m.Project,
			"users", m.Users)

		var rsp interfaces.CreateProjectRsp
		rsp.RequestId = m.RequestId
		rsp.Error = &interfaces.Error{Ok: true}
		err := s.context.Send(&rsp, message.Sender)
		if err != nil {
			s.context.Log().Warnw(
				"Failed to send create project response",
				"from", interfaces.ProjectsActorId,
				"to", message.Sender,
				"request_id", rsp.RequestId,
				"error", err)
		}

	case *interfaces.GetProjectUsersReq:
		s.context.Log().Debugw(
			"Received get project users request",
			"from", message.Sender,
			"to", interfaces.ProjectsActorId,
			"request_id", m.RequestId,
			"project", m.Project,
			"consistent", m.Consistent)

		var rsp interfaces.GetProjectUsersRsp
		rsp.RequestId = m.RequestId
		rsp.Error = &interfaces.Error{
			Ok:      false,
			Message: "Service is unavailable",
		}
		err := s.context.Send(&rsp, message.Sender)
		if err != nil {
			s.context.Log().Warnw(
				"Failed to send get project users response",
				"from", interfaces.ProjectsActorId,
				"to", message.Sender,
				"request_id", rsp.RequestId,
				"error", err)
		}

	default:
		s.context.Log().Warnw(
			"Ignore unexpected message type for project service",
			"from", message.Sender,
			"to", interfaces.ProjectsActorId,
			"message_type", reflect.TypeOf(message.Payload).String())
	}
}

func NewProjectsService(context interfaces.ActorContext) interfaces.Actor {
	return &ProjectsService{
		context: context,
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////

type UsersService struct {
	context interfaces.ActorContext
}

func (s *UsersService) Receive(message interfaces.Message) {
	switch m := message.Payload.(type) {
	case *interfaces.CreateUserReq:
		s.context.Log().Debugw(
			"Received create user request",
			"from", message.Sender,
			"to", interfaces.UsersActorId,
			"request_id", m.RequestId,
			"user", m.User,
			"projects", m.Projects,
			"sender", message.Sender)

		var rsp interfaces.CreateUserRsp
		rsp.RequestId = m.RequestId
		rsp.Error = &interfaces.Error{Ok: true}
		err := s.context.Send(&rsp, message.Sender)
		if err != nil {
			s.context.Log().Warnw(
				"Failed to send create user response",
				"from", interfaces.UsersActorId,
				"to", message.Sender,
				"request_id", rsp.RequestId,
				"error", err)
		}

	case *interfaces.GetUserProjectsReq:
		s.context.Log().Debugw(
			"Received get user projects request",
			"from", message.Sender,
			"to", interfaces.UsersActorId,
			"request_id", m.RequestId,
			"user", m.User,
			"consistent", m.Consistent)

		var rsp interfaces.GetUserProjectsRsp
		rsp.RequestId = m.RequestId
		rsp.Error = &interfaces.Error{
			Ok:      false,
			Message: "Service is unavailable",
		}

		err := s.context.Send(&rsp, message.Sender)
		if err != nil {
			s.context.Log().Warnw(
				"Failed to send get user projects response",
				"from", interfaces.UsersActorId,
				"to", message.Sender,
				"request_id", rsp.RequestId,
				"error", err)
		}

	default:
		s.context.Log().Warnw(
			"Ignore unexpected message type for users service",
			"from", message.Sender,
			"to", interfaces.UsersActorId,
			"message_type", reflect.TypeOf(message.Payload).String())
	}
}

func NewUsersService(context interfaces.ActorContext) interfaces.Actor {
	return &UsersService{
		context: context,
	}
}
