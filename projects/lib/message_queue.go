package lib

type MessageQueue []SentMessage

func (q MessageQueue) Len() int { return len(q) }

func (q MessageQueue) Less(i, j int) bool {
	return q[i].DeliverTimestamp < q[j].DeliverTimestamp
}

func (q MessageQueue) Swap(i, j int) {
	q[i], q[j] = q[j], q[i]
}

func (q *MessageQueue) Push(x interface{}) {
	sentMessage := x.(SentMessage)
	*q = append(*q, sentMessage)
}

func (q *MessageQueue) Pop() interface{} {
	old := *q
	n := len(old)
	item := old[n-1]
	*q = old[0 : n-1]
	return item
}
